# react-challenge-proximity

Challenge of the company proximity to assess knowledge about React

## Initialize the application with the script
```
sh dev-init.sh 
```

OR

Step by step

1) Go to the folder of the app
```
cd react-challenge-proximity/
```

2) Install dependencies
```
npm i
```

3) Run app
```
npm start
```

## Testing
To run the tests
```
npm test
```

## Demo
- https://react-challenge-proximity.vercel.app/

For this projects were used the followings libraries:

- axios 			https://www.npmjs.com/package/axios
- react-router-dom 	https://www.npmjs.com/package/react-router-dom
- redux 			https://www.npmjs.com/package/redux
- react-redux 		https://www.npmjs.com/package/react-redux
- redux-thunk 		https://www.npmjs.com/package/redux-thunk
- bootstrap			https://www.npmjs.com/package/bootstrap