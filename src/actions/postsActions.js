import postsApi from "../apis/postsApi";

export const getPosts = () => async dispatch => {

  const res = await postsApi.get();

  if (!res.error) {
    dispatch({
      type: 'GET_POSTS',
      payload: res.posts
    })
  } else {
    dispatch({
      type: 'GET_POSTS_ERROR',
      payload: res.error
    })
  }
};