import commentsApi from "../apis/commentsApi";

export const getComments = () => async dispatch => {

  const res = await commentsApi.get();
  
  if (!res.error) {
    dispatch({
      type: 'GET_COMMENTS',
      payload: res.comments
    })
  } else {
    dispatch({
      type: 'GET_COMMENTS_ERROR',
      payload: res.error
    })
  }
};

export const addComment = (comment) => dispatch => {
  
  dispatch({
    type: 'ADD_COMMENT',
    payload: comment
  })
};
