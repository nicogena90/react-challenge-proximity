const reducer = (state = {}, action) => {
  
  switch (action.type) {
       
    case 'GET_POSTS':
      return {
        ...state,
        posts: action.payload,
      }

    case 'GET_POSTS_ERROR':
      return {
        ...state,
        posts: [],
        fatalError: action.payload.toString()
      }

    default: 
      return state;

  }
}

export default reducer;