const reducer = (state = {}, action) => {
  
  switch (action.type) {
       
    case 'GET_COMMENTS':
      return {
        ...state,
        comments: action.payload,
      }

    case 'GET_COMMENTS_ERROR':
      return {
        ...state,
        posts: [],
        fatalError: action.payload.toString()
      }

    case 'ADD_COMMENT':
      return {
        ...state,
        comments: [...state.comments.slice(0, 0),
            action.payload,
            ...state.comments.slice(0)]
      }

    default: 
      return state;

  }
}

export default reducer;