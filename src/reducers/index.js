import { combineReducers } from 'redux'

import comments from './commentsReucers';
import posts from './postsReucers';

export default combineReducers({
  comments,
  posts
})