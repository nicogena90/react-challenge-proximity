import reducers from "./reducers";
import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";

// Estado inicial de la aplicación
const initialState = {
  posts: [],
  comments: [],
  fatalError: ""
}

const store = createStore(reducers, initialState, applyMiddleware(reduxThunk));

export default store;

