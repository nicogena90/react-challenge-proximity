import React from "react";
import "./styles.css";

export class Comments extends React.Component {

  commentBody = React.createRef();

  /**
   * Save the comment in the store
   */
  saveComment = () => {

    const { addComment, post } = this.props;

    // Create comment object
    const comment = {
      postId: post.id,
      name: "Anonymous",
      body: this.commentBody.current.value
    }

    // Check if the user fill the comment
    if (this.commentBody.current.value !== "") {
      addComment(comment);
      this.commentBody.current.value = "";
    }
  }

  render() {

    const { comments = [], post } = this.props;

    let postComments;

    // We filter the comments of the current post
    postComments = comments.filter( 
      comment => 
        comment.postId === Number(post.id))

    if (!postComments) return null; 

    return (
      <React.Fragment>
        <div className="comments-title">
          <h4>Comments</h4>
        </div>
        <div className="input-group mb-3">
          <input ref={this.commentBody} type="text" className="form-control" placeholder="New comment" aria-describedby="basic-addon2" />
          <div className="input-group-append">
            <button onClick={this.saveComment} className="btn btn-outline-secondary" type="button">Add Comment</button>
          </div>
        </div>
        <div className="comment-section">
          {postComments.map(comment => {
            return (
              <div key={comment.id} className="card">
                <div className="card-body">
                  <p className="card-text">{comment.body}</p>
                  <footer className="blockquote-footer comment-body">{comment.name}</footer>
                </div>
            </div>
            )
          })}
        </div>
      </React.Fragment >
    );
  }
}

export default Comments;
