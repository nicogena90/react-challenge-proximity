import React from "react";
import { connect } from "react-redux";
import Comments from "./Comments";
import { addComment } from "../../actions/commentsActions"
import "./styles.css";

export class Post extends React.Component {

  componentDidMount() {
    window.scrollTo(0, 0)
  }

  /**
   * Adds a comment to the store
   * @param {comment} comment 
   */
  addComment = (comment) => {
    const { addComment } = this.props;
    
    addComment(comment)
  }

  render() {

    const { posts = [], comments = [] } = this.props;

    let post;

    // We get the id of the post from the URL
    const postId = this.props.match.params.postId;

    if (posts.length > 0) {

      // We find the post from the collection
      post = posts.find( post => post.id === Number(postId));
    }

    if (!post) return null; 

    return (
      <React.Fragment>
        <div className="container-fluid">
          <div className="row">
            <div className="mx-auto col-md-8">
              <div className="card">
                <div className="card-body">
                  <h2>{post.title}</h2>
                  <p className="card-text post-body">{post.body}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="mx-auto col-md-6">
              <Comments 
                post={post}
                addComment={this.addComment}
                comments={comments}
              />
            </div>
          </div>
        </div>          
      </React.Fragment >
    );
  }
}

const mapStateToProps = ({
  comments: { comments },
  posts: { posts }
}) => ({
  comments,
  posts
});

export default connect(mapStateToProps, { addComment })(Post);
