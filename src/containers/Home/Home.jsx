import React from "react";
import { connect } from "react-redux";
import Posts from "./Posts";
import { Link } from "react-router-dom";

export class Home extends React.Component {

  render() {

    const { posts = [] } = this.props;
    return (
      <React.Fragment>
        <div className="container-fluid">
          { 
            posts.map(post => {
              return (
                <div key={post.id} className="row">
                  <div className="mx-auto col-md-8">
                    <Link to={`post/${post.id}`} style={{ textDecoration: "none" }}>
                      <Posts post={post}></Posts>
                    </Link>
                  </div>
                </div>
              )
            })
          }
        </div>
      </React.Fragment >
    );
  }
}

const mapStateToProps = ({
  posts: { posts },
}) => ({
  posts
});

export default connect(mapStateToProps, null)(Home);
