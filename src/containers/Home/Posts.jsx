import React from "react";
import "./styles.css";

export class Posts extends React.Component {
  render() {
    
    const { post = {} } = this.props;

    return (
      <React.Fragment>
        <div className="card">
          <div className="card-body">
            <h5 className="post-link">{post.title}</h5>
          </div>
        </div>
      </React.Fragment >
    );
  }
}

export default Posts;
