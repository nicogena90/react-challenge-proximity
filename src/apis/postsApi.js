import { callApi } from "./api";

const postsApi = () => {
  async function get(parameters) {
    try {
      const response = await callApi(
        `/posts`, {
          method: `GET`,
          parameters,
        }
      );

      return { posts: response.data, error: false };
    } catch (error) {
      return { posts: [], error };
    }
  }

  return {
    get,
  };
};

export default postsApi();
