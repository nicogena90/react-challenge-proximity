import axios from "axios";

const BASE_URL = "https://jsonplaceholder.typicode.com/";

export async function callApi(endpoint, options = {}) {
  let restdb = await axios.create({
    baseURL: BASE_URL,
    headers: {
      "Content-Type": "application/json;charset=UTF-8"
    },
  });

  switch (options.method) {
    case "GET":
      if (options.parameter)
        return await restdb.get(`${endpoint}?${options.parameter}`);
      else return await restdb.get(`${endpoint}`);
    default:
      break;
  }
}
