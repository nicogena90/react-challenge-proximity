import { callApi } from "./api";

const commentsApi = () => {
  async function get(parameters) {
    try {
      const response = await callApi(
        `/comments`, {
          method: `GET`,
          parameters,
        });
      return { comments: response.data, error: false };
    } catch (error) {
      return { comments: [], error };
    }
  }

  return {
    get,
  };
};

export default commentsApi();
