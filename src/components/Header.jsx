import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getPosts } from "../actions/postsActions"
import { getComments } from "../actions/commentsActions"

export class Header extends React.Component {

  componentDidMount() {
    this.props.getPosts();
    this.props.getComments();
  }

  render() {
    return (
      <React.Fragment>
        <div className="card">
          <div className="card-body">
          <Link to={`/`} style={{ textDecoration: "none" }}>
            Home
          </Link>
          </div>
        </div>
      </React.Fragment >
    );
  }
}

export default connect(null, { getPosts, getComments })(Header);
