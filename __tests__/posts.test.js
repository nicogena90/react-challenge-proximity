import Posts from "../src/containers/Home/Posts";
import React from "react";
import { mount } from "enzyme";

test("Posts component when is loaded", () => {
  const post = { id: 1, title: "Post Title" };
  const wrapper = mount(<Posts post={post}/>);

  const title = wrapper.find('.post-link');
  expect(title.text()).toBe('Post Title');
});
