import Comments from "../src/containers/Post/Comments";
import React from "react";
import { mount } from "enzyme";

test("Comments component when is loaded", () => {
  const comments = [{ id: 1, postId: 1, name: "Comment Body" }];
  const post = { id: 1, title: "Post Title" };

  const wrapper = mount(<Comments comments={comments} post={post}/>);

  const comment = wrapper.find('.comment-body');
  expect(comment.text()).toBe('Comment Body');
});
